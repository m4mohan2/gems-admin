export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      title: true,
      name: 'Management',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Users',
      url: '/users',
    },
    {
      name: 'Gems',
      url: '/gems',
      icon: 'icon-cursor',
      children: [
        {
          name: 'Gems',
          url: '/buttons/buttons',
          icon: 'icon-cursor',
        },
        {
          name: 'Gems Category',
          url: '/gems/gems-category',
          icon: 'icon-cursor',
        }
      ],
    },
    {
      divider: true,
    },
    {
      title: true,
      name: 'Extras',
    },
    
    {
      name: 'Disabled',
      url: '/dashboard',
      icon: 'icon-ban',
      attributes: { disabled: true },
    },
  ],
};
