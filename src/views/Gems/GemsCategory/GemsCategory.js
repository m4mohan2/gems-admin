import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

function EachRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 1 ? 'success' :
      status === 0 ? 'secondary' :
        status === 'Pending' ? 'warning' :
          status === 'Banned' ? 'danger' :
            'primary'
  }

  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{user.category_name}</Link></td>
      <td>{user.is_approved}</td>
      <td><Badge color={getBadge(user.active_status)}>{user.active_status === 1 ? 'Active' : 'Inactive'}</Badge></Link></td>
    </tr>
  )
}

class GemsCategory extends Component {

  state = {
    items:[],
    isLoaded:false,
  }

  constructor(props) {
    super(props);
  }

  async componentDidMount(){
    const url = "http://54.147.235.207/gems_uat/service/public/index.php/api/listCategory";
    const response = await fetch (url);
    const data = await response.json();

    console.log("response=>", data);
      
    this.setState({
      isLoaded:true,
      items:data
    })
      
  }

  render() {
    console.log("====>", this.state);

    var { isLoaded, items } = this.state;

    if (!isLoaded) {
      return (<div> Loading....</div>);

    } else {


      //const userList = items.filter((user) => user.id < 10)

      return (
        <div className="animated fadeIn">
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Users <small className="text-muted">example</small>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">id</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">role</th>
                        <th scope="col">status</th>
                      </tr>
                    </thead>
                    <tbody>
                      {items.map((user, index) =>
                        <EachRow key={index} user={user}/>
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }

  }
}

export default GemsCategory;
